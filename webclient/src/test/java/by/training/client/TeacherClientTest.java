package by.training.client;

import by.training.model.Lesson;
import by.training.model.Teacher;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TeacherClientTest {

    @Test
    public void testSendGetWhenObjectNotExists() throws Exception {
        assertNull("Object should not be found", new TeacherClient().sendGet
                (0));
    }

    @Test
    public void testSendGetWhenObjectExists() throws Exception {
        TeacherClient client = new TeacherClient();
        Teacher teacher = new Teacher();
        teacher.setId(111);
        teacher.setName("Jack");
        teacher.setBirthday(new Date());
        teacher.setLessons(new ArrayList<>());
        client.sendPost(teacher);
        assertEquals("Objects should be equal", teacher, client
                .sendGet(111));
    }

    @Test
    public void testPutWhenObjectNotModified() {
        TeacherClient client = new TeacherClient();
        Teacher teacher = new Teacher();
        teacher.setId(2222);
        teacher.setName("Jack");
        teacher.setBirthday(new Date());
        teacher.setLessons(new ArrayList<>());
        client.sendPost(teacher);
        assertNull("Should return null", client.sendPut(2222, teacher));
    }

    @Test
    public void testGetBusiestTeacher() {

        TeacherClient client = new TeacherClient();
        long busyId = 2232;
        List<Lesson> lessons = new ArrayList<>();
        lessons.add(new Lesson(1, "math", 1));
        lessons.add(new Lesson(2, "pt", 1));

        Teacher busyTeacher = new Teacher();
        Teacher teacher = new Teacher();

        busyTeacher.setId(busyId);
        busyTeacher.setName("Ivan");
        busyTeacher.setBirthday(new Date());
        busyTeacher.setLessons(lessons);

        teacher.setId(2322);
        teacher.setName("Jack");
        teacher.setBirthday(new Date());
        teacher.setLessons(new ArrayList<>());

        client.sendPost(busyTeacher);
        client.sendPost(teacher);

        assertEquals(busyTeacher, client.getBusiestTeacher());
    }

    @Test
    public void testSendPost() {
        TeacherClient client = new TeacherClient();
        Teacher teacher = new Teacher();
        teacher.setId(222);
        teacher.setName("Jack");
        teacher.setBirthday(new Date());
        teacher.setLessons(new ArrayList<>());
        assertEquals("Objects should be equal", teacher, client.sendPost
                (teacher));
        assertEquals("Server should contain an equal object", teacher,
                client.sendGet(222));
    }

    @Test
    public void testSendPut() {
        TeacherClient client = new TeacherClient();
        Teacher teacher = new Teacher();
        teacher.setId(333);
        teacher.setName("Jack");
        teacher.setBirthday(new Date());
        teacher.setLessons(new ArrayList<>());
        assertEquals("Objects should be equal", teacher, client.sendPut
                (teacher));
        assertEquals("Server should contain an equal object", teacher,
                client.sendGet(333));
    }

    @Test
    public void testSendDelete() {
        TeacherClient client = new TeacherClient();
        Teacher teacher = new Teacher();
        teacher.setId(444);
        teacher.setName("Jack");
        teacher.setBirthday(new Date());
        teacher.setLessons(new ArrayList<>());
        client.sendPost(teacher);
        client.sendDelete(444);
        assertNull(client.sendGet(444));
    }
}