package by.training.model;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "lesson")
@XmlType(propOrder = {"id", "name", "duration"})
public class Lesson {
    private long id;
    private String name;
    private int duration;

    public Lesson() {
    }

    public Lesson(long id, String name, int duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lesson lesson = (Lesson) o;

        if (getId() != lesson.getId()) return false;
        if (getDuration() != lesson.getDuration()) return false;
        if (getName() != null ? !getName().equals(lesson.getName()) : lesson.getName() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + getDuration();
        return result;
    }
}
