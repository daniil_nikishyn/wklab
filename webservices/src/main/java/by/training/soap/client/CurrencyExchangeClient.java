package by.training.soap.client;

import net.webservicex.Currency;
import net.webservicex.CurrencyConvertor;
import net.webservicex.CurrencyConvertorSoap;


public class CurrencyExchangeClient {

    private CurrencyConvertor service;
    private CurrencyConvertorSoap server;

    public CurrencyExchangeClient() {
        this.service = new CurrencyConvertor();
        this.server = service.getCurrencyConvertorSoap();
    }

    public double getConversion(double number, Currency fromCurrency,
                                Currency toCurrency) {

        return server.conversionRate(fromCurrency, toCurrency) * number;

    }
}
