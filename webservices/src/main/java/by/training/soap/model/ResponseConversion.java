package by.training.soap.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GetConversionResponse")
public class ResponseConversion {

    double amount;

    public ResponseConversion() {
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
