package by.training.soap.service;

import by.training.soap.client.CurrencyExchangeClient;
import by.training.soap.fault.IncorrectDataFault;
import by.training.soap.fault.IncorrectDataFaultBean;
import by.training.soap.model.RequestConversion;
import by.training.soap.model.ResponseConversion;
import net.webservicex.Currency;
import org.springframework.beans.factory.annotation.Autowired;

public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

    @Autowired
    private CurrencyExchangeClient exchangeClient;

    @Override
    public ResponseConversion getConversion(RequestConversion request) throws IncorrectDataFault {

        if (!(request.getFromCurrency() instanceof Currency) || !(request
                .getToCurrency() instanceof Currency)) {
            throw new IncorrectDataFault(new IncorrectDataFaultBean("One of the"
                    + " currencies is not RUB, USD or EUR!"));
        }

        double result = exchangeClient.getConversion(request.getNumber(),
                request.getFromCurrency(), request.getToCurrency());

        ResponseConversion response = new ResponseConversion();

        response.setAmount(result);

        return response;
    }

}
