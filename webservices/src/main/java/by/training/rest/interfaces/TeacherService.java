package by.training.rest.interfaces;

import by.training.rest.model.Teacher;
import org.springframework.context.annotation.Description;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_JSON)
public interface TeacherService {

    /**
     * Adds new Teacher object into the storage.
     * @param teacher
     * @return
     */
    @POST
    @Path("/teachers")
    Response addTeacher(Teacher teacher);

    /**
     * Adds new Teacher object into the storage.
     * @param teacher
     * @return
     */
    @PUT
    @Path("/teachers")
    Response addNewTeacher(Teacher teacher);

    /**
     * Updates requested Teacher object if it exists or creates the new one.
     * @param id
     * @param teacher
     * @return
     */
    @PUT
    @Path("/teachers/{id}")
    Response updateTeacher(@PathParam("id") long id, Teacher teacher);

    /**
     * Finds requested object in the storage.
     * @param id
     * @return
     */
    @GET
    @Path("/teachers/{id}")
    Response getTeacher(@PathParam("id") long id);

    /**
     * Finds Teacher object with the biggest lessons amount.
     * @return
     */
    @GET
    @Path("/teachers/busiest")
    Response getBusiestTeacher();

    /**
     * Removes Teacher object from the storage.
     * @param id
     * @return
     */
    @DELETE
    @Path("/teachers/{id}")
    Response deleteTeacher(@PathParam("id") long id);
}
