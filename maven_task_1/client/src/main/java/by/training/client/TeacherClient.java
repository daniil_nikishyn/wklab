package by.training.client;


import by.training.model.Teacher;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class TeacherClient {

    private static final String DEFAULT_URL =
            "http://localhost:8080/webservices/rest";
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final Logger LOGGER = LogManager.getLogger
            (TeacherClient.class);

    private static final int HTTP_CODE_NOT_FOUND = 404;
    private static final int HTTP_CODE_NOT_MODIFIED = 304;


    public Teacher sendGet(long id) {
        String url = DEFAULT_URL + "/teachers/" + id;
        return sendRequest("GET", url);
    }

    /**
     * Send GET HTTP request to get the Teacher with the biggest lessons amount.
     *
     * @return
     */
    public Teacher getBusiestTeacher() {
        String url = DEFAULT_URL + "/teachers/busiest";
        return sendRequest("GET", url);
    }

    public Teacher sendDelete(long id) {
        String url = DEFAULT_URL + "/teachers/" + id;
        return sendRequest("DELETE", url);
    }

    public Teacher sendPost(Teacher teacher) {
        String urlString = DEFAULT_URL + "/teachers/";
        return sendRequest(teacher, "POST", urlString);
    }

    public Teacher sendPut(Teacher teacher) {
        String urlString = DEFAULT_URL + "/teachers/";
        return sendRequest(teacher, "PUT", urlString);
    }

    public Teacher sendPut(long id, Teacher teacher) {
        String urlString = DEFAULT_URL + "/teachers/" + id;
        return sendRequest(teacher, "PUT", urlString);
    }

    /**
     * Sends PUT or POST HTTP request.
     *
     * @param teacher
     * @param requestMethod
     * @return
     */
    private static Teacher sendRequest(Teacher teacher, String requestMethod,
                                       String urlString) {


        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
        StringBuffer response = null;
        String json = gson.toJson(teacher, Teacher.class);

        try {
            URL obj = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // Setting basic post request
            con.setRequestMethod(requestMethod);
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json");

            // Send post request
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json);

            int responseCode = con.getResponseCode();
            LOGGER.info("Sending " + requestMethod + " request to URL : " + urlString);
            LOGGER.info("Data : " + json);
            LOGGER.info("Response Code : " + responseCode);
            if (responseCode == HTTP_CODE_NOT_MODIFIED) {
                return null;
            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return convertFromXml(response.toString());

    }

    /**
     * Sends GET or DELETE HTTP request.
     *
     * @param url
     * @param requestMethod
     * @return
     */
    private static Teacher sendRequest(String requestMethod, String url) {


        StringBuffer response = null;
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod(requestMethod);

            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            LOGGER.info("\nSending " + requestMethod + " request to URL : " + url);
            LOGGER.info("Response Code : " + responseCode);
            if (responseCode == HTTP_CODE_NOT_FOUND) {
                return null;
            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return convertFromXml(response.toString());
    }

    /**
     * Converts XML string to an object.
     *
     * @param xmlString XML string
     * @return Teacher object
     */
    private static Teacher convertFromXml(String xmlString) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Teacher.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(xmlString);
            Teacher teacher = (Teacher) unmarshaller.unmarshal(reader);
            return teacher;
        } catch (JAXBException e) {
            LOGGER.error(e);
        }
        return null;
    }
}
