package by.training.cache;


import by.training.model.Teacher;
import by.training.rest.interfaces.TeacherStorage;

import java.util.HashMap;
import java.util.Map;

public class TeacherStorageImpl implements TeacherStorage {

    private Map<Long, Teacher> storage = new HashMap<>();

    @Override
    public void put(Teacher teacher) {
        storage.put(teacher.getId(), teacher);
    }

    @Override
    public void update(long id, Teacher teacher) {
        storage.put(id, teacher);
    }

    @Override
    public Teacher get(long id) {
        return storage.get(id);
    }

    @Override
    public Teacher getBusiest() {
        int currentValue = 0;
        int prevValue = 0;
        long busiest = 0;
        for (Map.Entry<Long, Teacher> entry : storage.entrySet()) {
            currentValue = entry.getValue().getLessons().size();
            if (currentValue > prevValue) {
                busiest = entry.getKey();
            }
            prevValue = currentValue;
        }
        return storage.get(busiest);
    }

    @Override
    public void remove(long id) {
        storage.remove(id);
    }
}
