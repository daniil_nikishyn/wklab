package by.training.service;

import by.training.model.Teacher;
import by.training.rest.interfaces.TeacherService;
import by.training.rest.interfaces.TeacherStorage;

import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;

public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherStorage teacherStorage;

    @Override
    public Response addTeacher(Teacher teacher) {
        teacherStorage.put(teacher);
        return Response.status(Response.Status.CREATED).entity(teacher).build();
    }

    @Override
    public Response addNewTeacher(Teacher teacher) {
        return addTeacher(teacher);
    }

    @Override
    public Response updateTeacher(long id, Teacher teacher) {
        Teacher currentTeacher = teacherStorage.get(id);
        if (currentTeacher == null) {
            teacherStorage.put(teacher);
            return Response.status(Response.Status.CREATED).entity(teacher).build();
        }
        if (currentTeacher.equals(teacher)) {
            return Response.notModified().build();
        }
        teacherStorage.update(id, teacher);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response getTeacher(long id) {
        Teacher teacher = teacherStorage.get(id);
        if (teacher == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(teacher).build();
    }

    @Override
    public Response getBusiestTeacher() {
        Teacher teacher = teacherStorage.getBusiest();
        if (teacher == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(teacher).build();
    }

    @Override
    public Response deleteTeacher(long id) {
        Teacher teacher = teacherStorage.get(id);
        if (teacher == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        teacherStorage.remove(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

}
