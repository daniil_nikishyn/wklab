package by.training.rest.interfaces;


import by.training.model.Teacher;

public interface TeacherStorage {

    /**
     * Puts Teacher object in the storage.
     *
     * @param teacher
     */
    void put(Teacher teacher);

    /**
     * Replaces an old object value to the new one.
     *
     * @param id
     * @param teacher
     */
    void update(long id, Teacher teacher);

    /**
     * Gets an object from the storage.
     *
     * @param id
     * @return
     */
    Teacher get(long id);

    /**
     * Gets the Teacher with the biggest amount of lessons.
     *
     * @return
     */
    Teacher getBusiest();

    /**
     * Removes an object from the storage.
     *
     * @param id
     */
    void remove(long id);
}
