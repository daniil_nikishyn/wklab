package by.training.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "teacher")
@XmlType(propOrder = {"id", "name", "birthday", "lessons"})
public class Teacher {
    private long id;
    private String name;
    private Date birthday;
    private List<Lesson> lessons;

    public Teacher() {
    }

    public Teacher(long id, String name, Date birthday, List<Lesson> lessons) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.lessons = lessons;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @XmlElementWrapper
    @XmlElement(name = "lesson")
    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        if (getId() != teacher.getId()) return false;
        if (getName() != null ? !getName().equals(teacher.getName()) : teacher.getName() != null)
            return false;
        if (getBirthday() != null ? !getBirthday().equals(teacher.getBirthday()) : teacher.getBirthday() != null)
            return false;
        if (getLessons() != null ? !getLessons().equals(teacher.getLessons()) : teacher.getLessons() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getBirthday() != null ? getBirthday().hashCode() : 0);
        result = 31 * result + (getLessons() != null ? getLessons().hashCode() : 0);
        return result;
    }
}
