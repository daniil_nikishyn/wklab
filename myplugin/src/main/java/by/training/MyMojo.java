package by.training;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

@Mojo(name = "copy")
public class MyMojo extends AbstractMojo {

    @Parameter
    private File inputFile;

    @Parameter
    private File outputFile;


    public void execute() throws MojoExecutionException {

        getLog().info("Copy file...");

        FileChannel sourceChannel = null;
        FileChannel destChannel = null;

        try {
            sourceChannel = new FileInputStream(inputFile).getChannel();
            destChannel = new FileOutputStream(outputFile).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } catch (IOException e) {
            getLog().error(e);
        } finally {
            try {
                sourceChannel.close();
                destChannel.close();
            } catch (IOException e) {
                getLog().error(e);
            }
        }
    }
}