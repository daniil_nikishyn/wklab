package by.training.soap.service;

import by.training.soap.client.CurrencyExchangeClient;
import by.training.soap.fault.IncorrectDataFault;
import by.training.soap.fault.IncorrectDataFaultBean;
import by.training.soap.model.RequestConversion;
import by.training.soap.model.ResponseConversion;
import net.webservicex.Currency;

import java.util.HashSet;
import java.util.Set;


public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

    private static Set<Currency> allowedCurrncs;

    static {
        allowedCurrncs = new HashSet<>();
        allowedCurrncs.add(Currency.USD);
        allowedCurrncs.add(Currency.EUR);
        allowedCurrncs.add(Currency.RUB);
    }


    @Override
    public ResponseConversion getConversion(RequestConversion request) throws IncorrectDataFault {

        CurrencyExchangeClient exchangeClient = new CurrencyExchangeClient();

        if (!(allowedCurrncs.contains(request.getFromCurrency())) || !(allowedCurrncs.contains(request
                .getToCurrency()))) {
            throw new IncorrectDataFault(new IncorrectDataFaultBean("One of the"
                    + " currencies is not RUB, USD or EUR!"));
        }

        double result = exchangeClient.getConversion(request.getNumber(),
                request.getFromCurrency(), request.getToCurrency());

        ResponseConversion response = new ResponseConversion();

        response.setAmount(result);

        return response;
    }

}
