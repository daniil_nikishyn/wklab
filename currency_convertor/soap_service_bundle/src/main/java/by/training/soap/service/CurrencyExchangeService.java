package by.training.soap.service;

import by.training.soap.fault.IncorrectDataFault;
import by.training.soap.model.RequestConversion;
import by.training.soap.model.ResponseConversion;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public interface CurrencyExchangeService {

    @WebMethod
    @WebResult(name = "ConversionResult")
    ResponseConversion getConversion(@WebParam(name = "RequestConversion")
                                             RequestConversion request)
            throws IncorrectDataFault;
}
