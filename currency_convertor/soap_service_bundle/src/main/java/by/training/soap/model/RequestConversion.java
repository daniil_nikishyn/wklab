package by.training.soap.model;

import net.webservicex.Currency;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GetConversionRequest")
public class RequestConversion {

    double number;
    Currency fromCurrency;
    Currency toCurrency;

    public RequestConversion() {
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public Currency getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(Currency fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public Currency getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(Currency toCurrency) {
        this.toCurrency = toCurrency;
    }
}
